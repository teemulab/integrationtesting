import express from 'express';
import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';
import routes from '../src/routes.js';

const API_URL = "http://localhost:3000/api/v1";
const app = express();
let server;

describe("REST API routes", () => {
    before((done) => {
        app.use('/api/v1', routes);
        server = app.listen(3000, () => done());
    });
    it("can get response", async () => {
        const response = await fetch(`${API_URL}/`);
        expect(response.ok).to.be.true;
    });
    it("should convert HEX to RGB correctly", async () => {
        const hex = encodeURIComponent('#ff8800');
        const response = await fetch(`${API_URL}/hex-to-rgb?hex=${hex}`);
        expect(response.ok).to.be.true;
        const json = await response.json();
        expect(json).to.deep.equal({ r: 255, g: 136, b: 0 });
    });
    after((done) => {
        server.close(() => done());
    });
});