import { describe, it } from "mocha";
import { expect } from "chai";
import { hex_to_rgb } from "../src/converter.js"

describe("HEX-to-RGB Converter", () => {
    it("is a function", () => {
        expect(hex_to_rgb).to.be.a('function');
    });
    it("converts #000000 to RGB correctly", () => {
        expect(hex_to_rgb("#000000")).to.deep.equal({ r: 0, g: 0, b: 0 });
    });
    it("converts #ff8800 to RGB correctly", () => {
        expect(hex_to_rgb("#ff8800")).to.deep.equal({ r: 255, g: 136, b: 0 });
    });
});
