import { Router } from 'express';
import { hex_to_rgb } from './converter.js';

const routes = Router();

// Endpoint GET '{{api_url}}/'
routes.get('/', (req, res) => {
    res.status(200).send("Welcome!");
});

// Endpoint GET /hex-to-rgb?hex=#ff8800
routes.get('/hex-to-rgb', (req, res) => {
    const hex = req.query.hex;
    if (!hex) {
        return res.status(400).json({ error: "No hex value provided" });
    }

    const rgb = hex_to_rgb(hex);
    if (rgb) {
        res.status(200).json(rgb);
    } else {
        res.status(400).json({ error: "Invalid hex format" });
    }
});


export default routes;
