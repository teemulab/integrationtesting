/**
 * Validates hex color format.
 * @param {string} hex 
 * @returns {boolean}
 */
const isValidHex = (hex) => {
    const regex = /^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/;
    return regex.test(hex);
};

/**
 * Converts a single hex component to an RGB component.
 * @param {string} hexComponent 
 * @returns {number}
 */
const hexComponentToRGB = (hexComponent) => {
    return parseInt(hexComponent, 16);
};

/**
 * Converts a hex color string to an RGB object.
 * @param {string} hex
 * @returns {object|null} 
 */
export const hex_to_rgb = (hex) => {
    if (!isValidHex(hex)) {
        return null;
    }
    if (hex.startsWith('#')) {
        hex = hex.slice(1);
    }
    
    if (hex.length === 3) {
        hex = hex.split('').map(char => char + char).join('');
    }
    const r = hexComponentToRGB(hex.substring(0, 2));
    const g = hexComponentToRGB(hex.substring(2, 4));
    const b = hexComponentToRGB(hex.substring(4, 6));
    
    return { r, g, b };
};
